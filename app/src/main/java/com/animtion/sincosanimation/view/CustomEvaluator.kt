package com.animtion.sincosanimation.view

import android.animation.TypeEvaluator
import com.animtion.custombezieranimation.view.ViewPath
import com.animtion.custombezieranimation.view.ViewPoint

/**
 * DESCRIPTION
 * com.animtion.sincosanimation.view
 * Created by six.sev on 2017/8/3.
 */
class CustomEvaluator : TypeEvaluator<ViewPoint>{

    var startX = 0f
    var startY = 0f
    var x = 0f
    var y = 0f

    override fun evaluate(fraction: Float, startValue: ViewPoint?, endValue: ViewPoint?): ViewPoint {
        when(endValue!!.operation){
            ViewPath.MOVE -> {
                x = endValue!!.x
                y = endValue!!.y
            }
            ViewPath.QUAD -> {
                var temp = 1 - fraction
                startX = if(startValue!!.operation == ViewPath.QUAD) startValue.x1 else startValue.x
                startY = if(startValue!!.operation == ViewPath.QUAD) startValue.y1 else startValue.y
                x = temp * temp * startX + 2 * fraction * temp * endValue!!.x + fraction * fraction * endValue!!.x1
                y  = temp * temp * startY + 2 * fraction * temp * endValue!!.y + fraction * fraction * endValue!!.y1
            }
            else -> {
                x = endValue!!.x
                y = endValue!!.y
            }
        }
        return ViewPoint(x, y)
    }
}