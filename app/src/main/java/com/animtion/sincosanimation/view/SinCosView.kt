package com.animtion.sincosanimation.view

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import com.animtion.sincosanimation.util.UIUtils

/**
 * DESCRIPTION
 * com.animtion.sincosanimation.view
 * Created by six.sev on 2017/8/7.
 */
class SinCosView : View{
    var mPaint: Paint? = null
    var mPath: Path? = null
    var sinCount = 0
    var mWidth = 0
    var mHeitht = 0
    val wave_width = UIUtils.dp2px(context, 60f)
    val wave_height = UIUtils.dp2px(context, 25f)

    constructor(context: Context) : super(context){
        init()
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){
        init()
    }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        init()
    }

    fun init(){
        mPaint = Paint()
        mPaint!!.isAntiAlias = true
        mPaint!!.color = Color.parseColor("#ffff0000")
        mPaint!!.style = Paint.Style.FILL_AND_STROKE

        mPath = Path()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        mWidth = MeasureSpec.getSize(widthMeasureSpec)
        mHeitht = MeasureSpec.getSize(heightMeasureSpec)
        oriY = (mHeitht / 2).toFloat()
        //屏幕左边多绘制一个波长
        sinCount = mWidth / wave_width + 1
    }

    override fun draw(canvas: Canvas?) {
        super.draw(canvas)
        drawSinCos(canvas)
    }

    var dx = 0f
    var oriY = 0f
    private fun drawSinCos(canvas: Canvas?) {
        mPath!!.moveTo(-wave_width.toFloat() + dx, oriY)
        for (index in 0..sinCount){
            /*mPath!!.quadTo(index + wave_width / 4 + dx, - wave_height / 2 + oriY, index + wave_width / 2 + dx, oriY)
            mPath!!.quadTo(index + wave_width * 3/ 4 + dx, wave_height / 2 + oriY, index + wave_width + dx, oriY)*/
            if(index == 0){
                mPath!!.quadTo((-wave_width * 3 / 4).toFloat(), -wave_height / 2 + oriY, (-wave_width / 2).toFloat(), oriY)
                mPath!!.quadTo((-wave_width / 4).toFloat(), wave_height / 2 + oriY, 0f, oriY)
            }else {
                mPath!!.quadTo((wave_width / 4) * (1 + index * 4).toFloat(), -wave_height / 2 + oriY, (wave_width / 2) * (1 + index * 2).toFloat(), oriY)
                mPath!!.quadTo((wave_width / 4) * (3 + index * 4).toFloat(), wave_height / 2 + oriY, (wave_width / 2) * (2 + index * 2).toFloat(), oriY)
            }
        }
        mPath!!.lineTo(mWidth.toFloat(), mHeitht.toFloat())
        mPath!!.lineTo(-wave_width.toFloat(), mHeitht.toFloat())
        mPath!!.close()
        canvas!!.drawPath(mPath, mPaint)
    }

    fun start(){
        var objAni = ValueAnimator.ofFloat(0f, 1f)
        objAni.duration = 3000
        objAni.repeatCount = ValueAnimator.INFINITE
        objAni.addUpdateListener { animation ->
            var temp: Float = animation!!.animatedValue as Float
            dx = temp * wave_width
            postInvalidate()
        }
        objAni.start()
    }

}