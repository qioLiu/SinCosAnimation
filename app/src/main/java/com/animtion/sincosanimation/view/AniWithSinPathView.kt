package com.animtion.sincosanimation.view

import android.animation.ObjectAnimator
import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.widget.ImageView
import android.widget.RelativeLayout
import com.animtion.custombezieranimation.view.ViewPath
import com.animtion.custombezieranimation.view.ViewPoint
import com.animtion.sincosanimation.R
import com.animtion.sincosanimation.util.UIUtils

/**
 * DESCRIPTION view move with path
 * Created by six.sev on 2017/8/3.
 */
class AniWithSinPathView : RelativeLayout {

    val dp500 = UIUtils.dp2px(context, 500f)
    val DEFAULT_WAVE_WIDTH = UIUtils.dp2px(context, 150f)
    val DEFAULT_WAVE_HEIGHT = UIUtils.dp2px(context, 150f)

    //视图的宽度
    var mWidth = 0
    //视图高度
    var mHeight = 0
    //sin/cos一段完整曲线宽度
    var waveWidth = DEFAULT_WAVE_WIDTH
    //sin/cos一段完整曲线高度
    var waveHeight = DEFAULT_WAVE_HEIGHT
    //在视图宽度内能显示的完整曲线的个数
    var sinCount = 0

    //视图移动的路径
    var path: ViewPath? = null
    //视图 如需自定义可以设置为styleable自己去获取对应的ID来加载
    var image: ImageView? = null

    constructor(context: Context?) : super(context){
        initStyle(null)
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs){
        initStyle(attrs)
    }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr){
        initStyle(attrs)
    }

    fun initStyle(attrs: AttributeSet?){
        var typeArr = context!!.obtainStyledAttributes(attrs, R.styleable.AniWithSinPathView)
        waveWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, typeArr.getInteger(R.styleable.AniWithSinPathView_wave_with, DEFAULT_WAVE_WIDTH).toFloat(), context.resources.displayMetrics).toInt()
        waveHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, typeArr.getInteger(R.styleable.AniWithSinPathView_wave_height, DEFAULT_WAVE_HEIGHT).toFloat(), context.resources.displayMetrics).toInt()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val height = MeasureSpec.getSize(heightMeasureSpec)

        when(widthMode){
            MeasureSpec.EXACTLY -> mWidth = width
            MeasureSpec.AT_MOST -> mWidth = dp500
            else -> mWidth = dp500
        }
        when(heightMode){
            MeasureSpec.EXACTLY -> mHeight = height
            MeasureSpec.AT_MOST -> mHeight = dp500
            else -> mHeight = dp500
        }

        sinCount = mWidth / waveWidth

        setMeasuredDimension(mWidth, mHeight)

        initPath()
    }

    private fun initPath() {
        path = ViewPath()
        path!!.moveTo(0f, 0f)
        for (index in 0..sinCount - 1){
            // 计算不同index情况下对应的起始X,Y和结束X.Y 。 利用两段二阶贝塞尔表示正弦。两个高度反过来则是余弦
            /**
             * 正弦
             */
            /*path!!.quadTo((waveWidth / 4) * (1 + index * 4).toFloat(), -waveHeight / 2.toFloat(), (waveWidth / 2) * (1 + index * 2).toFloat(), 0f)
            path!!.quadTo((waveWidth / 4) * (3 + index * 4).toFloat(), waveWidth / 2.toFloat(), (waveWidth / 2) * (2 + index * 2).toFloat(), 0f)*/
            /**
             *余弦
             */
            path!!.quadTo((waveWidth / 4) * (1 + index * 4).toFloat(), waveHeight / 2.toFloat(), (waveWidth / 2) * (1 + index * 2).toFloat(), 0f)
            path!!.quadTo((waveWidth / 4) * (3 + index * 4).toFloat(), -waveWidth / 2.toFloat(), (waveWidth / 2) * (2 + index * 2).toFloat(), 0f)
        }
        //startAnimation()
    }

    fun initViews(){
        var params: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        params.addRule(CENTER_VERTICAL)

        image = ImageView(context)
        image!!.setImageResource(R.drawable.red)
        image!!.layoutParams = params
        addView(image)

        var objAni = ObjectAnimator.ofObject(ViewObj(image!!), "location", CustomEvaluator(), *(path!!.getPointArray()))
        objAni.duration = 2000
        objAni.start()
    }

    fun startAnimation(){
        removeAllViews()
        initViews()
    }

    //ofObject对应的set函数
    inner class ViewObj(var view: ImageView) {
        fun setLocation(point: ViewPoint){
            view.translationX = point.x
            view.translationY = point.y
        }
    }
}