package com.animtion.custombezieranimation.view

/**
 * create for custom point
 * Created by six.sev on 2017/8/1.
 */
class ViewPoint {
    var x = 0f
    var y = 0f
    var x1 = 0f
    var y1 = 0f
    var x2 = 0f
    var y2 = 0f

    var operation = 0

    constructor()



    constructor(x: Float, y: Float, operation: Int) {
        this.x = x
        this.y = y
        this.operation = operation
    }

    constructor(x: Float, y: Float, x1: Float, y1: Float, operation: Int) {
        this.x = x
        this.y = y
        this.x1 = x1
        this.y1 = y1
        this.operation = operation
    }

    constructor(x: Float, y: Float, x1: Float, y1: Float, x2: Float, y2: Float, operation: Int) {
        this.x = x
        this.y = y
        this.x1 = x1
        this.y1 = y1
        this.x2 = x2
        this.y2 = y2
        this.operation = operation
    }

    constructor(x: Float, y: Float) {
        this.x = x
        this.y = y
    }


    companion object{
        fun movePoint(x: Float, y: Float, operation: Int): ViewPoint{
            return ViewPoint(x, y, operation)
        }

        fun linePoint(x: Float, y: Float, operation: Int): ViewPoint{
            return ViewPoint(x, y, operation)
        }

        fun quadPoint(x: Float, y: Float, x1: Float, y1: Float, operation: Int): ViewPoint{
            return ViewPoint(x, y, x1, y1, operation)
        }

        fun cubicPoint(x: Float, y: Float, x1: Float, y1: Float, x2: Float, y2: Float,operation: Int): ViewPoint{
            return ViewPoint(x, y, x1, y1, x2, y2, operation)
        }
    }

}