package com.animtion.sincosanimation.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.animtion.sincosanimation.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        show.setOnClickListener { sin_view.start() }
    }
}
