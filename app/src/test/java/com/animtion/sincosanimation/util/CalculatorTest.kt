package com.animtion.sincosanimation.util

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

/**
 * DESCRIPTION
 * com.animtion.sincosanimation.util
 * Created by six.sev on 2017/8/8.
 */
class CalculatorTest {
    var mCalculator: Calculator? = null
    @Before
    fun setUp() {
        mCalculator = Calculator()
    }

    @Test
    fun add() {
        assertEquals(6, mCalculator!!.add(1, 5))
    }

}